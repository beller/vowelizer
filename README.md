# Introduction:
Vowelizer is a Max For Live plugin for modifying formants in real time.
Vowelizer pops, dims, boosts, or changes the color of vowels in the voice.
Vowelizer concentrates the energy of the spectrum in areas close to the formants by resampling the spectral envelope by a nonlinear function. Vowelizer can therefore shift a formant, without using formant waveform filtering, thus preserving the energy of the input signal at all times. Vowelizer can be used in the studio to enhance voice recordings, for example, and on stage as a real-time sound effect. Vowelizer uses SuperVP technology for spectral envelope estimation by TrueEnvelop and its high quality resampling by a nonlinear or piecewise linear function. The absence of the "vocoder effect" allows Vowelizer to modify the voice while maintaining its natural appearance.

# Version and acknowledgments:
* Author: Greg Beller
* Date: 01-03-2022
* Version 1.0
* Credits: 
  * Thanks to the Ircam Research teams ISMM and AnaSyn for SuperVP for Max and Yin.
  * This device uses the Ircam Template for Max For Live devices.
* Dependencies: This device uses latest versions of SuperVP for Max and Max Sound Box
  * yin~ (pitch analysis), version 11/2021 (18) by ISMM Team after Cheveigne/Kawahara, IRCAM - Center Pompidou
  * supervp.trans~, SuperVP for Max/MSP, version 2.18.8 (11/2021) entirely based on SuperVP (version 2.103.6) by Axel Roebel. Max/MSP integration by ISMM Team, IRCAM - Center Pompidou
